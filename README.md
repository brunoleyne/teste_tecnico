# Processo Seletivo - Back End 123milhas

## Instalando Aplicação

- Renomear arquivo .env.example para .env e configurar conforme ambiente (Usuario MySQL)
- Realizar comando abaixo para instalar dependências
```
composer install
```


## Informações Sobre Arquivos e Lógicas

1 - Dentro \database\migrations\2021_01_31_230046_create_flights_table.php, existe migration para criar as tabela flight, necessário para armazenar informações da API. Utilize comando:

```
php artisan migrate
```

2 - Dentro \app\Console\Commands\CacheFlights.php, existe a função responsável por consultar API 123milhas e armazenar no banco. Utilize comando abaixo para popular tabela de flights baseado na API 123milhas:
```
php artisan cache:flights
```
 
3- Utilize a rota /api/flights para visualizar o agrupamento de voos Arquivo Lógico em : \app\Http\Controllers\FlightController.php

4 - API disponível em http://brunleyne.net/api/flights

5 - Dentro do arquivo "123Milhas Bruno Leyne.postman_collection.json" na raiz da aplicação está a collection para as chamadas Postman