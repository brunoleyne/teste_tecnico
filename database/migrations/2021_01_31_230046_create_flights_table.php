<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->id();
            $table->string('cia', 100);
            $table->string('fare', 3);
            $table->string('flightNumber', 7)->unique();
            $table->string('origin', 3);
            $table->string('destination', 3);
            $table->date('departureDate');
            $table->date('arrivalDate');
            $table->time('departureTime', 0);
            $table->time('arrivalTime', 0);
            $table->unsignedTinyInteger('classService');
            $table->decimal('price', 10, 2);
            $table->unsignedSmallInteger('tax');
            $table->boolean('outbound')->index();
            $table->boolean('inbound')->index();
            $table->time('duration', 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flights');
    }
}
