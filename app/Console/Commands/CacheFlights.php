<?php

namespace App\Console\Commands;

use App\Models\Flight;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class CacheFlights extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:flights';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Preenche a tabela flights com voos da API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info("Realizando cache de voos");
        try {
            $response = Http::get('http://prova.123milhas.net/api/flights')->json();

            //Realiza store de voos no DB
            DB::transaction(function () use ($response) {
                foreach ($response as $voo) {
                    if (!isset($voo['flightNumber'])) {
                        continue;
                    }

                    $vooDB = Flight::firstOrCreate(
                        ['flightNumber' => $voo['flightNumber']],
                        [
                            'cia' => $voo['cia'],
                            'fare' => $voo['fare'],
                            'origin' => $voo['origin'],
                            'destination' => $voo['destination'],
                            'departureDate' => $voo['departureDate'],
                            'arrivalDate' => $voo['arrivalDate'],
                            'departureTime' => $voo['departureTime'],
                            'arrivalTime' => $voo['arrivalTime'],
                            'classService' => $voo['classService'],
                            'price' => $voo['price'],
                            'tax' => $voo['tax'],
                            'outbound' => $voo['outbound'],
                            'inbound' => $voo['inbound'],
                            'duration' => $voo['duration'],
                        ]
                    );
                    $vooDB->save();
                }
            });

            $this->info("Cache Finalizado com Sucesso");
        } catch (\Exception $e) {
            $this->error("Ocorreu um erro fatal: {$e->getMessage()}");
        }
    }
}
