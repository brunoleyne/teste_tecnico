<?php

namespace App\Http\Controllers;

use App\Models\Flight;

class FlightController extends Controller
{
    public function index()
    {
        try {
            $flights = Flight::all();
            if ($flights->count() == 0) {
                return response()->json([
                    'flights' => [],
                    'groups' => [],
                    'totalGroups' => 0,
                    'totalFlights' => 0,
                    'cheapestPrice' => null,
                    'cheapestGroup' =>null,
                ], 200);
            }

            //Agrupa voos por outbound/inbound, fare e price
            foreach ($flights as $flight) {
                $flightsResponse[] = $flight->flightNumber;
                if ($flight->outbound == 1) {
                    $group['outbound'][$flight->fare][$flight->price][] = $flight;
                } else if ($flight->inbound == 1) {
                    $group['inbound'][$flight->fare][$flight->price][] = $flight;
                }
            }
    
    
            //Realiza as combinações de voos por outbound/inbound, fare e price
            $groupIndex = 0;
            foreach ($group['outbound'] as $fare => $flightPrices) {
    
                foreach ($flightPrices as $price => $flight) {
                    $flightCombinationByFarePrice[$groupIndex]['idas'][] = $group['outbound'][$fare][$price];
    
                    foreach ($group['inbound'][$fare] as $inboundFlightPrices => $inboundFlights) {
                        $flightCombinationByFarePrice[$groupIndex]['voltas'][] = $group['inbound'][$fare][$inboundFlightPrices];
                    }
                    $groupIndex++;
                }
    
            }
    
            //Cria index com mais informações para as combinações 
            $uniqueId = 0;
            foreach ($flightCombinationByFarePrice as $voosCombinacoes) {
                foreach ($voosCombinacoes['idas'] as $idas) {
                    foreach ($voosCombinacoes['voltas'] as $voltas) {
                        $allFlightCombinations[$uniqueId]['uniqueId'] = $uniqueId;
                        $allFlightCombinations[$uniqueId]['totalPrice'] = $idas[0]->price + $voltas[0]->price;
                        $allFlightCombinations[$uniqueId]['outbound'] = $idas;
                        $allFlightCombinations[$uniqueId]['inbound'] = $voltas;
                        $uniqueId++;
                    }
                }
            }
    
            //Ordena array por preço
            usort($allFlightCombinations, function($a, $b) {
                return $a['totalPrice'] <=> $b['totalPrice'];
            });
    
            //Determina quantide de voos únicos
            $totalFlights = 0;
            foreach ($allFlightCombinations as $fly){
                $totalFlights += count($fly['outbound']) * count($fly['inbound']);
            }
    
            return response()->json([
                'flights' => $flightsResponse,
                'groups' => $allFlightCombinations,
                'totalGroups' => count($allFlightCombinations),
                'totalFlights' => $totalFlights,
                'cheapestPrice' => $allFlightCombinations[0]['totalPrice'],
                'cheapestGroup' => $allFlightCombinations[0]['uniqueId'],
            ], 200);
        }
        catch (\Exception $e) {
            return response()->json([
                'data' => 'Ocorreu um erro fatal',
                'error' => $e->getMessage()
            ], 500);
        }

    }
}
