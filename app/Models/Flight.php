<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Flight extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cia',
        'fare',
        'flightNumber',
        'origin',
        'destination',
        'departureDate',
        'arrivalDate',
        'departureTime',
        'arrivalTime',
        'classService',
        'price',
        'tax',
        'outbound',
        'inbound',
        'duration',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Accessor Data Padrão PT-BR.
     *
     * @param  string  $value
     * @return string
     */
    public function getDepartureDateAttribute($value)
    {
        return (new Carbon($value))->format('d/m/Y');
    }
    /**
     * Mutator Data Padrão PT-BR.
     *
     * @param  string  $value
     * @return void
     */
    public function setDepartureDateAttribute($value)
    {
        $this->attributes['departureDate'] = implode("-", array_reverse(explode("/", $value)));
    }
    /**
     * Accessor Data Padrão PT-BR.
     *
     * @param  string  $value
     * @return string
     */
    public function getArrivalDateAttribute($value)
    {
        return (new Carbon($value))->format('d/m/Y');
    }
    /**
     * Mutator Data Padrão PT-BR.
     *
     * @param  string  $value
     * @return void
     */
    public function setArrivalDateAttribute($value)
    {
        $this->attributes['arrivalDate'] = implode("-", array_reverse(explode("/", $value)));

    }

}
